package com.github.contactlutforrahman.flutter_qr_bar_scanner;

import java.util.List;

public interface QrReaderCallbacks {
    void qrRead(List<String> data);
}
